﻿package main

import (
	"fmt"
	"sort"
)

func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

func IntSliceToString(sl []int) string {
	res := ""

	for _, el := range sl {
		res = res + fmt.Sprint(el)
	}
	return string(res)
}

func MergeSlices(s1 []float32, s2 []int32) []int {
	res := make([]int, len(s1)+len(s2))

	for i, el := range s1 {
		res[i] = int(el)
	}

	for i, el := range s2 {
		res[len(s1)+i] = int(el)
	}

	return res
}

func GetMapValuesSortedByKey(mp map[int]string) []string {
	res := make([]string, len(mp))

	var tmp_keys []int = []int{}

	for k := range mp {
		tmp_keys = append(tmp_keys, k)
	}
	sort.Ints(tmp_keys)

	for i, k := range tmp_keys {

		res[i] = mp[k]
	}

	return res
}
